#!bin/bash
#Installing programs for Ubuntu 12.04 server
START=$(date +%s)

echo;
clear && echo 'executing-apt-get update' && apt-get update && echo "done"
echo;

echo;
echo 'Installing tasksel..' && apt-get install tasksel -y && echo "done"
echo;

echo;
echo 'Installing Lamp-server...'
sudo tasksel install lamp-server && echo "..done"
echo;

echo;
echo "Installing php libraries..." && \
apt-get install php5-cli -y && \
apt-get install php5-gd -y && \
sudo apt-get install php5-intl -y && \
echo "Installing php libraries...done"
echo;

echo;
echo "Installing Bind..." && \
sudo aptitude install bind9 bind9utils -y && \
echo "Installing Bind...done"
echo;

echo;
echo "Installing openjdk-7-jre..." && \
sudo apt-get install openjdk-7-jre -y && \
echo "Installing openjdk-7-jre..done"
echo;

echo;
echo "Installing openjdk-7-jdk..." && \
sudo apt-get install openjdk-7-jdk -y && \
echo "Installing openjdk-7-jdk..done"
echo;

echo;
echo "Installing Jenkins..." && \
wget -q -O - http://pkg.jenkins-ci.org/debian/jenkins-ci.org.key | sudo apt-key add - && \
sudo sh -c 'echo deb http://pkg.jenkins-ci.org/debian binary/ > /etc/apt/sources.list.d/jenkins.list' && \
sudo apt-get update && \
sudo apt-get install -y jenkins && \
sudo /etc/init.d/jenkins start && \
echo "Installing Jenkins...done"
echo;

echo;
echo "Installing Postfix..." && \
sudo apt-get install postfix -y && \
echo "Installing Postfix...done"
echo;

echo;
echo 'Installing curl...' && sudo apt-get install curl && \
echo 'Installing curl...DONE'
echo;

# INSTALL NODE.JS AND NPM
echo 'Executing: sudo apt-get install python-software-properties python g++ make'
sudo apt-get install python-software-properties python g++ make -y && echo "done"
echo;

echo 'Executing: sudo add-apt-repository ppa:chris-lea/node.js'
sudo add-apt-repository ppa:chris-lea/node.js -y && echo "done"
echo;

echo 'Executing: sudo apt-get update'
sudo apt-get update && echo "done"
echo;

echo 'Executing: sudo apt-get install nodejs'
sudo apt-get install nodejs && echo "done"
echo;

echo 'Executing: node --version'
node --version && echo "done"
echo;

echo 'Executing: npm --version'
npm --version && echo "done"
echo;
#INSTALL NODE AND NPM...DONE

echo;
echo 'Installing ruby...' && \
sudo apt-get install ruby1.9.1 && \
echo 'Installing ruby...DONE'
echo;

echo;
echo 'Installing sass...' && \
sudo apt-get install sass && \
echo 'Installing sass...DONE'
echo;

echo;
echo 'Installing compass...' && \
sudo gem install compass && \
echo 'Installing compass...DONE'
echo;

#FOR SYMFONY 
echo;
echo 'Installing acl...' && \
sudo apt-get install acl && \
echo 'Installing acl...DONE'
echo;

echo;
echo 'Executing: sudo npm install -g uglify-js'
sudo npm install -g uglify-js && echo "done"
echo;

echo 'Executing: sudo npm install -g uglifycss'
sudo npm install -g uglifycss && echo "done"
echo;

echo 'Your path uglify is:'
npm bin -g
echo;

echo;
echo 'Installing Phpmyadmin...' && \
sudo apt-get install phpmyadmin && \
echo 'Installing phpmyadmin...done'
echo;

echo;
echo 'Enable apache rewrite module...' && \
sudo a2enmod rewrite && \
echo 'Enable apache rewrite module..done '
echo;

END=$(date +%s)
DIFF=$(( $END - $START ))
echo "It took $DIFF seconds to execute these commands!"
echo;
