#!bin/bash

getserverip(){
        SERVER_IP=$(ifconfig  | grep 'inet addr:'| grep -v '127.0.0.*' | cut -d: -f2 | awk '{ print $1}')
        echo $SERVER_IP
}

symfonyCachePermissions() {
	 projectPath=$projectPath'/web' && \
	 ProjectDirectoryIndex='app.php' && \
	 rm -rf app/cache/* && \
	 rm -rf app/logs/* && \
	 APACHEUSER=`ps aux | grep -E '[a]pache|[h]ttpd|[_]www|[w]ww-data' | grep -v root | head -1 | cut -d\  -f1` && \
	 sudo setfacl -R -m u:"$APACHEUSER":rwX -m u:`whoami`:rwX app/cache app/logs && \
	 sudo setfacl -dR -m u:"$APACHEUSER":rwX -m u:`whoami`:rwX app/cache app/logs && \
	 echo;
}

ProjectDirectoryIndex='index.php' && \

echo 'Please enter subdomain name: '
read subdomain
echo 'Please paste repository url (for git clone) :'
read gitUrl
cd /home && \
sudo git clone $gitUrl $subdomain && \
projectPath="/home/$subdomain" && \
echo 'New project path is :'$projectPath && \
cd $projectPath && \
echo;

#CHECKING IF PROJECT IS A SYMFONY2 ONE...
echo 'Is this project in Symfony2 ? [Y,n]'
read key
case "$key" in
'Y'|''|'y')
    echo 'enter pressed'
    symfonyCachePermissions
;;
'n'|'N')
    echo;
    echo 'Please enter the new project"s relative path (' $projectPath ') :' && \
    read projectRoot && \
    if [ "$projectRoot" = "" ]; then
    	echo;
    else
    	projectPath=$projectPath'/'$projectRoot
    fi
    echo $projectPath

;;
*)
esac

echo "$subdomain.aelius.info.\tIN\tA\t$(getserverip)" >> /var/lib/bind/aelius.info.hosts && \
echo '<VirtualHost *:80>\nServerName '$subdomain.aelius.info'\nDocumentRoot '$projectPath'\nDirectoryIndex '$ProjectDirectoryIndex'\n<Directory "'$projectPath'">\nallow from all\nOptions +Indexes\n</Directory>\n</VirtualHost>' >> /etc/apache2/sites-available/$subdomain.conf && \

#RESTARTING SERVICES
sudo a2ensite $subdomain.conf && \
sudo /etc/init.d/bind9 restart && \
sudo service apache2 restart && \

echo;